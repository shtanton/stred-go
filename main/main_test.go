package main

import (
	"strings"
	"testing"
)

const miscInput string = `{"something":{"nested":"Here is my test value"},"array":["Hello","world","these","are","values"],"people":[{"first_name":"Charlie","last_name":"Johnson","age":22},{"first_name":"Tom","last_name":"Johnson","age":18},{"first_name":"Charlie","last_name":"Chaplin","age":122},{"first_name":"John","last_name":"Johnson","age":48}]}`
const mixedArray string = `{"array":["first",null,3,{"name":"second"},"third"]}`
const mixedArray2 string = `{"array":["first",null,3,"second",{"name":"third"}]}`

func TestSpecificCases(t *testing.T) {
	type test struct {
		name string
		program string
		quiet bool
		input string
		expected string
	}

	tests := []test {
		{
			name: "Verbose Extract",
			program: `s/#(~(people)~$_@(1$_#(~(first_name)~$_.|(..$_){-0})-|(..$_){-0})-|(..$_){-0})-/p`,
			quiet: true,
			input: miscInput,
			expected: `"Tom"`,
		},
		{
			name: "Extract",
			program: `s/#("people"$_ @(1 $_#("first_name"$_ .)-)-)-/p`,
			quiet: true,
			input: miscInput,
			expected: `"Tom"`,
		},
		{
			name: "Simple Extract",
			program: "s/#(\"people\" @(1 #(\"first_name\" (.$a))-)-)-$_ `$a`/p",
			quiet: true,
			input: miscInput,
			expected: `"Tom"`,
		},
		{
			name: "Larger Extract",
			program: "s/#(\"people\" @(2 (.$a))-)-$_ `$a`/p",
			quiet: true,
			input: miscInput,
			expected: `{"first_name":"Charlie","last_name":"Chaplin","age":122}`,
		},
		{
			name: "Extract ages",
			program: "s/#(\"people\"$_ :(#(\"age\"$_ .)-):)-/p",
			quiet: true,
			input: miscInput,
			expected: `[22,18,122,48]`,
		},
		{
			name: "Low memory count people",
			program: "aX/#(\"people\" :(#()#):)#$_ `1`/o es/#()#/{ xs/.{-0}+/p }",
			quiet: true,
			input: miscInput,
			expected: "4",
		},
		{
			name: "Get full names",
			program: "s/#(\"people\"$_ .)-/{ s/:():/p as/:(#()#):/{ xdx } s/:(#((\"first_name\" | \"last_name\") .)#)-/X es/@(.#()-)-/{ xs/(#(\"first_name\" \".{-0}$a\")# | #(\"last_name\" \".{-0}$b\")# | .){-0}$_ `\"$a $b\"`/Xxs/-(..)@/p } }",
			quiet: true,
			input: miscInput,
			expected: `["Charlie Johnson","Tom Johnson","Charlie Chaplin","John Johnson"]`,
		},
		{
			name: "Get full names 2",
			program: "s/#(\"people\"$_ .)-/{ s/:():/p as/:(#()#):/{ xdx } X/:(#((\"first_name\" | \"last_name\") .)#)-/o es/@(.#()-)-/{ xX/(#(\"first_name\" \".{-0}$a\")# | #(\"last_name\" \".{-0}$b\")# | .){-0}$_ `\"$a $b\"`/xs/-(..)@/p } }",
			quiet: true,
			input: miscInput,
			expected: `["Charlie Johnson","Tom Johnson","Charlie Chaplin","John Johnson"]`,
		},
		{
			name: "Change full names in place",
			program: "s/#(\"people\" @(. #(\"first_name\" .)#)@)#/{ Nms/#(\"people\" @(. (#(\"first_name\" \".{-0}$a\" \"last_name\" \".{-0}$b\")#$_) `#(\"name\" \"$a $b\")#`)@)#/ }",
			input: miscInput,
			expected: `{"something":{"nested":"Here is my test value"},"array":["Hello","world","these","are","values"],"people":[{"name":"Charlie Johnson","age":22},{"name":"Tom Johnson","age":18},{"name":"Charlie Chaplin","age":122},{"name":"John Johnson","age":48}]}`,
		},
		{
			name: "Get full names with substitute next command",
			program: "s/#( \"people\"$_ :( #( \"first_name\"$_ . )- )- )-/{ N/#( \"people\"$_ :( #( \"last_name\"$_ . )- )- )-/{ s/-( -( ~(.{-0}` `)- ~(.{-0})- )~ ):/p }}",
			quiet: true,
			input: miscInput,
			expected: `["Charlie Johnson","Tom Johnson","Charlie Chaplin","John Johnson"]`,
		},
		{
			name: "Get full names with merge full command",
			program: "s/#(\"people\"$_ :(): )-/p M/#( \"people\" @( . #()# )@ )#/{ s/#( \"people\"$_ @( . #[ \"first_name\" \".{-0}$a\" | \"last_name\" \".{-0}$b\" | .. $_]- `\"$a $b\"` )@ )-/p }",
			quiet: true,
			input: miscInput,
			expected: `["Charlie Johnson","Tom Johnson","Charlie Chaplin","John Johnson"]`,
		},
		{
			name: "Verbose concat array values",
			program: "as/#( \"array\"$_ :(): )-/{ :s N/#( .$_ . )-/{ es/.{-0}:():/be mbs } :em s/:( -( ~(.{-0}` `)-{-0} ~(.{-0})- )~ )-/p }",
			quiet: true,
			input: miscInput,
			expected: `"Hello world these are values"`,
		},
		{
			name: "Short concat array values",
			 program: "M/#( \"array\" :(): )#/{ s/#( \"array\"$_ :( .{-0} )- )-/ s/-( ~(.{-0}` `)-{-0} ~(.{-0})- )~/p }",
			quiet: true,
			input: miscInput,
			expected: `"Hello world these are values"`,
		},
		{
			name: "Drop first element of array",
			program: `s/#( "people" @( 0 . )@ )#/d`,
			input: miscInput,
			expected: `{"something":{"nested":"Here is my test value"},"array":["Hello","world","these","are","values"],"people":[{"first_name":"Tom","last_name":"Johnson","age":18},{"first_name":"Charlie","last_name":"Chaplin","age":122},{"first_name":"John","last_name":"Johnson","age":48}]}`,
		},
		{
			name: "Drop last element of array",
			program: `M/#( "people" @( . , )@ )#/{ Ed }`,
			input: miscInput,
			expected: `{"something":{"nested":"Here is my test value"},"array":["Hello","world","these","are","values"],"people":[{"first_name":"Charlie","last_name":"Johnson","age":22},{"first_name":"Tom","last_name":"Johnson","age":18},{"first_name":"Charlie","last_name":"Chaplin","age":122}]}`,
		},
		{
			name: "Drop last element of simple array",
			program: `s/#( "array" @( . . )@ )#/{ Ed }`,
			input: miscInput,
			expected: `{"something":{"nested":"Here is my test value"},"array":["Hello","world","these","are"],"people":[{"first_name":"Charlie","last_name":"Johnson","age":22},{"first_name":"Tom","last_name":"Johnson","age":18},{"first_name":"Charlie","last_name":"Chaplin","age":122},{"first_name":"John","last_name":"Johnson","age":48}]}`,
		},
		{
			name: "Drop last element of mixed array",
			program: `M/#( "array" @( . , )@ )#/{ Ed }`,
			input: mixedArray,
			expected: `{"array":["first",null,3,{"name":"second"}]}`,
		},
		{
			name: "Drop last element of mixed array 2",
			program: `M/#( "array" @( . , )@ )#/{ Ed }`,
			input: mixedArray2,
			expected: `{"array":["first",null,3,"second"]}`,
		},
		{
			name: "Prepend to array",
			program: "as/#( \"array\" :( `\"First\"` ): )#/",
			input: miscInput,
			expected: `{"something":{"nested":"Here is my test value"},"array":["First","Hello","world","these","are","values"],"people":[{"first_name":"Charlie","last_name":"Johnson","age":22},{"first_name":"Tom","last_name":"Johnson","age":18},{"first_name":"Charlie","last_name":"Chaplin","age":122},{"first_name":"John","last_name":"Johnson","age":48}]}`,
		},
		{
			name: "Append to array",
			program: "es/#( \"array\" :( `\"Last\"` ): )#/",
			input: miscInput,
			expected: `{"something":{"nested":"Here is my test value"},"array":["Hello","world","these","are","values","Last"],"people":[{"first_name":"Charlie","last_name":"Johnson","age":22},{"first_name":"Tom","last_name":"Johnson","age":18},{"first_name":"Charlie","last_name":"Chaplin","age":122},{"first_name":"John","last_name":"Johnson","age":48}]}`,
		},
	}

	for _, test := range tests {
		t.Logf("Running test: %s", test.name)

		var output strings.Builder
		run(config {
			quiet: test.quiet,
			program: test.program,
			in: strings.NewReader(test.input),
			out: &output,
		})
		
		if output.String() != test.expected {
			t.Errorf("Ran '%s' and expected %s but got %s", test.program, test.expected, output.String())
		}
	}
}
