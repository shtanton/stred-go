package json

import (
	"bufio"
	"main/walk"
	"strings"
	"testing"
)

func TestNavigateTo(t *testing.T) {
	type testCase struct {
		fromPath []walk.PathSegment
		fromState JSONWriterState
		toKeep int
		toPath []walk.PathSegment
		toState JSONWriterState
		expected string
	}

	tests := []testCase {
		{
			fromPath: []walk.PathSegment{},
			fromState: JSONWriterStateBeforeValue,
			toKeep: 0,
			toPath: []walk.PathSegment{"a", "b", "c"},
			toState: JSONWriterStateBeforeValue,
			expected: `{"a":{"b":{"c":`,
		},
		{
			fromPath: []walk.PathSegment{},
			fromState: JSONWriterStateBeforeValue,
			toKeep: 0,
			toPath: []walk.PathSegment{0, "a", "a", 0, 1},
			toState: JSONWriterStateInMap,
			expected: `[{"a":{"a":[[{`,
		},
		{
			fromPath: []walk.PathSegment{},
			fromState: JSONWriterStateInMap,
			toKeep: 0,
			toPath: []walk.PathSegment{},
			toState: JSONWriterStateInArray,
			expected: "}\n[",
		},
		{
			fromPath: []walk.PathSegment{0, 0},
			fromState: JSONWriterStateBeforeValue,
			toKeep: 2,
			toPath: []walk.PathSegment{"a"},
			toState: JSONWriterStateInArray,
			expected: `{"a":[`,
		},
		{
			fromPath: []walk.PathSegment{"a", "b"},
			fromState: JSONWriterStateAfterValue,
			toKeep: 1,
			toPath: []walk.PathSegment{"c"},
			toState: JSONWriterStateBeforeValue,
			expected: `,"c":`,
		},
		{
			fromPath: []walk.PathSegment{0, "a"},
			fromState: JSONWriterStateInArray,
			toKeep: 0,
			toPath: []walk.PathSegment{"b", 1},
			toState: JSONWriterStateInMap,
			expected: `]}]` + "\n" + `{"b":[{`,
		},
		{
			fromPath: []walk.PathSegment{"a", "b", "c", "d", "e"},
			fromState: JSONWriterStateAfterValue,
			toKeep: 2,
			toPath: []walk.PathSegment{"f", "g", "h"},
			toState: JSONWriterStateBeforeValue,
			expected: `}},"f":{"g":{"h":`,
		},
		{
			fromPath: []walk.PathSegment{"a", 0, "b"},
			fromState: JSONWriterStateAfterValue,
			toKeep: 2,
			toPath: []walk.PathSegment{0},
			toState: JSONWriterStateBeforeValue,
			expected: `},[`,
		},
	}

	for i, test := range tests {
		var writer strings.Builder
		jsonWriter := &JSONWriter {
			path: test.fromPath,
			writer: bufio.NewWriter(&writer),
			state: test.fromState,
		}
		jsonWriter.navigateTo(
			test.toKeep,
			test.toPath,
			test.toState,
		)
		jsonWriter.writer.Flush()
		res := writer.String()
		if res != test.expected {
			t.Errorf(`Test %d: Expected '%s' found '%s'`, i, test.expected, res)
		}
	}
}

func TestWrite(t *testing.T) {
	type testCase struct {
		values []walk.Value
		expected string
	}

	tests := []testCase {
		{
			values: []walk.Value {
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue {{
						Key: "b",
						Value: walk.StringValue("c"),
					}},
				}},
			},
			expected: `{"a":{"b":"c"}}`,
		},
		{
			values: []walk.Value {
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue {{
						Key: "b",
						Value: walk.StringValue("c"),
					}},
				}},
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue {{
						Key: "d",
						Value: walk.NullValue{},
					}},
				}},
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue{},
				}},
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue {{
						Key: "e",
						Value: walk.NumberValue(-4.3),
					}},
				}},
			},
			expected: `{"a":{"b":"c","d":null,"e":-4.3}}`,
		},
		{
			values: []walk.Value {
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue{{
						Key: "aa",
						Value: walk.StringValue("aav"),
					}},
				}},
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue{},
				}, {
					Key: "b",
					Value: walk.MapValue {{
						Key: "bb",
						Value: walk.StringValue("bbv"),
					}},
				}, {
					Key: "c",
					Value: walk.MapValue{},
				}},
				walk.MapValue {{
					Key: "c",
					Value: walk.MapValue {{
						Key: "cc",
						Value: walk.StringValue("ccv"),
					}},
				}},
			},
			expected: `{"a":{"aa":"aav"},"b":{"bb":"bbv"},"c":{"cc":"ccv"}}`,
		},
		{
			values: []walk.Value {
				walk.ArrayValue {{
					Index: 0,
					Value: walk.ArrayValue {{
						Index: 5,
						Value: walk.NumberValue(100),
					}},
				}},
				walk.ArrayValue {{
					Index: 0,
					Value: walk.ArrayValue{},
				}, {
					Index: 0,
					Value: walk.ArrayValue{},
				}},
				walk.ArrayValue {{
					Index: 0,
					Value: walk.NullValue{},
				}, {
					Index: 0,
					Value: walk.ArrayValue{},
				}},
				walk.ArrayValue {{
					Index: 0,
					Value: walk.ArrayValue{},
				}},
				walk.ArrayValue {{
					Index: 0,
					Value: walk.ArrayValue {{
						Index: 200,
						Value: walk.NumberValue(200),
					}},
				}},
			},
			expected: `[[100],[],null,[200]]`,
		},
		{
			values: []walk.Value {
				walk.MapValue {{
					Key: "a",
					Value: walk.MapValue {{
						Key: "b",
						Value: walk.StringValue("map"),
					}},
				}},
				walk.MapValue {{
					Key: "a",
					Value: walk.ArrayValue {{
						Index: 0,
						Value: walk.StringValue("array"),
					}},
				}},
			},
			expected: `{"a":{"b":"map"},"a":["array"]}`,
		},
		{
			values: []walk.Value {
				walk.ArrayValue {{
					Index: 0,
					Value: walk.ArrayValue {{
						Index: 0,
						Value: walk.ArrayValue {{
							Index: 1,
							Value: walk.StringValue("a"),
						}},
					}},
				}},
				walk.ArrayValue {{
					Index: 0,
					Value: walk.ArrayValue {{
						Index: 1,
						Value: walk.ArrayValue{},
					}},
				}},
			},
			expected: `[[["a"],[]]]`,
		},
		{
			values: []walk.Value {
				walk.ArrayValue {{
					Index: 0,
					Value: walk.MapValue {{
						Key: "a",
						Value: walk.StringValue("a"),
					}},
				}},
				walk.ArrayValue {{
					Index: 0,
					Value: walk.MapValue{},
				}},
			},
			expected: `[{"a":"a"}]`,
		},
	}

	for i, test := range tests {
		var writer strings.Builder
		jsonWriter := &JSONWriter {
			path: nil,
			writer: bufio.NewWriter(&writer),
			state: JSONWriterStateBeforeValue,
		}

		for _, value := range test.values {
			jsonWriter.Write(value)
		}
		jsonWriter.AssertDone()

		jsonWriter.writer.Flush()
		res := writer.String()
		if res != test.expected {
			t.Errorf(`Test %d: Expected '%s' found '%s'`, i, test.expected, res)
		}
	}
}
