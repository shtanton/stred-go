package walk

type StredReader interface {
	Read() (WalkItem, error)
	AssertDone()
}

type StredWriter interface {
	Write(Value) error
	AssertDone()
}
