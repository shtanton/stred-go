package subex

import (
	"main/walk"
)

type valueFilter interface {
	valueFilter(value walk.Value) bool
}

type selectScalarFilter struct {
	scalar walk.Scalar
}
func (scalar selectScalarFilter) valueFilter(value walk.Value) bool {
	return value == scalar.scalar
}

type anyNumberFilter struct {}
func (_ anyNumberFilter) valueFilter(value walk.Value) bool {
	_, isNumber := value.(walk.NumberValue)
	return isNumber
}

type anyBoolFilter struct {}
func (_ anyBoolFilter) valueFilter(value walk.Value) bool {
	_, isBool := value.(walk.BoolValue)
	return isBool
}

type simpleValueFilter struct {}
func (_ simpleValueFilter) valueFilter(value walk.Value) bool {
	switch value := value.(type) {
	case walk.NullValue:
		return true
	case walk.BoolValue:
		return true
	case walk.NumberValue:
		return true
	case walk.StringValue:
		return true
	case walk.ArrayValue:
		return len(value) == 0
	case walk.MapValue:
		return len(value) == 0
	default:
		panic("Invalid value type")
	}
}

type anyValueFilter struct {}
func (_ anyValueFilter) valueFilter(value walk.Value) bool {
	return true
}

type anyArrayFilter struct {}
func (_ anyArrayFilter) valueFilter(value walk.Value) bool {
	_, isArray := value.(walk.ArrayValue)
	return isArray
}

type anyMapFilter struct {}
func (_ anyMapFilter) valueFilter(value walk.Value) bool {
	_, isMap := value.(walk.MapValue)
	return isMap
}

type anyStringFilter struct {}
func (_ anyStringFilter) valueFilter(value walk.Value) bool {
	_, isString := value.(walk.StringValue)
	return isString
}


type runeFilter interface {
	runeFilter(r rune) bool
}

type anyRuneFilter struct {}
func (_ anyRuneFilter) runeFilter(r rune) bool {
	return true
}

type selectRuneFilter struct {
	r rune
}
func (f selectRuneFilter) runeFilter(r rune) bool {
	return f.r == r
}
