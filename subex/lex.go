package subex

import (
	"unicode/utf8"
)

const eof rune = -1
type StringRuneReader struct {
	input string
	pos, width int
}
func (l *StringRuneReader) Next() rune {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	var r rune
	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	return r
}
func (l *StringRuneReader) Rewind() {
	l.pos -= l.width
}

func NewStringRuneReader(input string) RuneReader {
	return &StringRuneReader {
		input: input,
		pos: 0,
		width: 0,
	}
}
